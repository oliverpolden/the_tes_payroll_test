# The Tes Payroll Test

Displays a table of salary and bonus payment dates for the next 12 months.

Visit: /payroll/dates

## Assumptions

* Displays the next 12 months. (Could be the current year)
* To start from the current month.
* In a real project, appropriate permissions and roles would need to be created 
and/or assigned.