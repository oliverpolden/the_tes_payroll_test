<?php
/**
 * User: oliver
 * Date: 07/11/2018
 * Time: 15:53
 */

namespace Drupal\Tests\the_tes_payroll_test\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\the_tes_payroll_test\PayrollDate;

/**
 * Class PayrollDatesTest
 *
 * @group the_tes_payroll_test
 *
 * @package Drupal\Tests\the_tes_payroll_test\Unit
 */
class PayrollDatesTest extends UnitTestCase {


  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests last working day in months.
   */
  public function testSalaryDate() {
    $datetime = new \DateTime();
    $dates = [
      [ // Test today: 7/11/2018
        'expected' => 30,
        'date' => '7/11/2018',
      ],
      [ // Test March 2019. Last day is 31st (Sunday), Friday is 29th.
        'expected' => 29,
        'date' => '1/3/2019',
      ],
      [ // Test August 2019. Last day is 31st (Saturday), Friday is 30th.
        'expected' => 30,
        'date' => '1/8/2019',
      ],
      [ // Test August 2019. Last day is 31st (Saturday), Friday is 30th. Test with last day as input.
        'expected' => 30,
        'date' => '31/8/2019',
      ],
      [ // Test January 2020. Last day is 31st (Friday). Last day is a weekday.
        'expected' => 31,
        'date' => '1/1/2020',
      ],
      [ // Test February 2020. Last day is 29th (Saturday). This is a leap year.
        'expected' => 28,
        'date' => '1/2/2020',
      ],
    ];

    foreach ($dates as $date) {
      $actual_date = $datetime->createFromFormat('d/m/Y', $date['date']);
      $salaryDate = new PayrollDate($actual_date);
      $actual_day = $salaryDate->getSalaryDay();
      $this->assertEquals($date['expected'], $actual_day);
    }
  }


  /**
   * Tests bonus dates are on the 15th or the next Wednesday if the 15th is on
   * a weekend.
   */
  public function testBonusDate() {
    $datetime = new \DateTime();
    $dates = [
      [ // Test today: 7/11/2018. 15th is a Thursday.
        'expected' => 15,
        'date' => '7/11/2018',
      ],
      [ // Test March 2019. 15th is a Friday.
        'expected' => 15,
        'date' => '1/3/2019',
      ],
      [ // Test August 2019. 15th is a Thursday.
        'expected' => 15,
        'date' => '1/8/2019',
      ],
      [ // Test August 2019. Test with last day as input.
        'expected' => 15,
        'date' => '31/8/2019',
      ],
      [ // Test January 2020. 15th is a Wednesday.
        'expected' => 15,
        'date' => '1/1/2020',
      ],
      [ // Test February 2020. 15th is a Saturday. This is a leap year.
        'expected' => 19,
        'date' => '1/2/2020',
      ],
      [ // Test a Sunday being the 15th.
        'expected' => 18,
        'date' => '1/3/2020',
      ],
    ];

    foreach ($dates as $date) {
      $actual_date = $datetime->createFromFormat('d/m/Y', $date['date']);
      $bonusDate = new PayrollDate($actual_date);
      $actual_day = $bonusDate->getBonusDay();
      $this->assertEquals($date['expected'], $actual_day);
    }
  }
}
