<?php
/**
* @file
* Contains \Drupal\clt\Controller\ResultsController.
*/

namespace Drupal\the_tes_payroll_test\Controller;

use Drupal\Core\Controller\ControllerBase;

class PayrollController extends ControllerBase {
  /**
   * Returns a page with payroll dates.
   *
   * @return array
   *   A simple renderable array.
   *
   * @throws \Exception
   */
  public function datesPage() {
    $months = the_tes_payroll_test_payroll_dates();

    // Get headers.
    // This isn't great because it assumes columns are always in the same order.
    $header = [];
    foreach ($months as $month) {
      foreach ($month as $column_header => $data) {
        if (!in_array($column_header, $header)) {
          $header[] = $column_header;
        }
      }
    }

    $element = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $months,
      //'#markup' => 'Hello, world',
    ];

    return $element;
  }
}
