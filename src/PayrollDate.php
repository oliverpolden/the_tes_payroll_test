<?php

namespace Drupal\the_tes_payroll_test;

//use Drupal\Core\Datetime\Element\Datetime;

/**
 * The traveller object.
 */
class PayrollDate {

  /**
   * The date in the month we are checking.
   *
   * @var \Datetime
   */
  protected $date;

  /**
   * The date the salary is paid.
   *
   * @var \Datetime
   */
  protected $salaryDate;

  /**
   * The date the bonus is paid.
   *
   * @var \Datetime
   */
  protected $bonusDate;

  public function __construct(\Datetime $date) {
    $this->date = $date;
    $this->setSalaryDate();
    $this->setBonusDate();
  }

  /**
   * Returns the last day of the month unless it's a Soturday or Sunday,
   * in which case return the Friday. (Ignores public holidays)
   */
  public function setSalaryDate() {
    // Get the last day of the month.
    $salary_date = clone $this->date;
    $salary_date->modify('last day of this month');

    // Check if the date falls on a weekend. Adjust to previous Friday if it does.
    if ($this->dateIsWeekend($salary_date)) {
      $salary_date->modify('-1 Weekday'); // Find the previous weekday.
    }

    $this->salaryDate = $salary_date;
  }

  /**
   * Returns the day of the month to pay the salary.
   *
   * @return int
   *  The day of the month to pay the salary.
   */
  public function getSalaryDay() {
    return $this->getDay($this->salaryDate);
  }

  /**
   * Returns the 15th of the month unless it's a weekend in which case
   * return the next Wednesday.
   */
  public function setBonusDate() {
    // Get the 15th of the month.
    $bonus_date = clone $this->date;
    $bonus_date->modify('last day of previous month')->modify('+15 day');

    // Check if the date falls on a weekend. Adjust to next Wednesday if it does.
    if ($this->dateIsWeekend($bonus_date)) {
      if ($bonus_date->format('w') == 0) {
        $bonus_date->modify('+3 day');
      }
      else {
        $bonus_date->modify('+4 day');
      }
    }

    $this->bonusDate = $bonus_date;
  }

  /**
   * Returns the day of the month to pay the bonus.
   *
   * @return int
   *  The day of the month to pay the bonus.
   */
  public function getBonusDay() {
    return $this->getDay($this->bonusDate);
  }

  /**
   * Returns the day of the month from a date.
   *
   * @param $date
   *
   * @return int
   *  The day of the month.
   */
  public function getDay(\Datetime $date) {
    return $date->format('d');
  }

  /**
   * Checks if a date falls on a weekend.
   *
   * @var $date: The date to check
   *
   * @return TRUE if it does fall on a weekend, otherwise FALSE.
   */
  public function dateIsWeekend(\Datetime $date) {
    $day_of_week = $date->format('w');
    if ($day_of_week == 0 || $day_of_week == 6) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
}
